//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/slick.js
//= library/wow.js


$(document).ready(function () {

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});
	/* START preloader*/
	$("#cube-loader").css("display", "none");
	/* END preloader */

	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		console.log( $(this).hasClass("active") );

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
		} else {
			$(".navigation__content").removeClass("active");
		}
	});
	/* END откртие меню*/

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn_to_top").addClass("active");
		} else {
			$(".btn_to_top").removeClass("active");
		}

	});
	$('.btn_to_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */


	/* START calc event */
	$(".btn__calc").on("click", function (e) {
		//e.preventDefault();
		//e.stopPropagation();
		$(this).parent().parent().find(".btn__calc").removeClass("active");
		$(this).parent().parent().parent().parent().find(".calc__preview").html("");

		$(this).toggleClass("active");

		if ( !($(this).data("id") == "") ) {
			if( $(this).hasClass("active") ) {
				$(this).parent().parent().parent().parent().find(".calc__preview").append( "<img class='over img-fluid img__"+ $(this).data("id") +"' src="+ $(this).data("img-src") +" alt='img'>" );
			} else {
				//var id = ".img__"+$(this).data("id");
				//$(".calc__preview").find(id).remove();

				$(this).parent().parent().parent().parent().find(".calc__preview").append( "<img class='over img__"+ $(this).data("id") +"' src="+ $(this).data("img-src") +" alt='img'>" );
			}
		}
	});

	$('#modalCalc').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;
		var recipient = button.data('whatever') ;
		//var modal = $(this);
		console.log(recipient);
		//modal.find('.modal-body .calc__val').val(recipient);
	})
	/* END calc event */





});


